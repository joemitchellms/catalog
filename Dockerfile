FROM openjdk:11.0.6-jre-buster
VOLUME /tmp
COPY ./build/libs/*.jar app.jar
COPY ./src/main/resources/catalog.json ./src/main/resources/catalog.json
ENV SPRING_OUTPUT_ANSI_ENABLED=ALWAYS
ADD entrypoint.sh entrypoint.sh
RUN echo "$PWD"
ENTRYPOINT ["./entrypoint.sh"]