package shop.velox.catalog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.core.mapping.ExposureConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import shop.velox.catalog.model.CatalogEntity;
import shop.velox.catalog.model.ProductEntity;
import shop.velox.catalog.service.CatalogService;

@SpringBootApplication
public class CatalogApplication implements RepositoryRestConfigurer {

	private CatalogService catalogService;

	public CatalogApplication(@Autowired CatalogService catalogService) {
    this.catalogService = catalogService;
  }

	public static void main(String[] args) {
		SpringApplication.run(CatalogApplication.class, args);
	}

	@Override
	public void configureRepositoryRestConfiguration(RepositoryRestConfiguration restConfig, CorsRegistry cors) {
			restConfig.exposeIdsFor(ProductEntity.class);

			ExposureConfiguration config = restConfig.getExposureConfiguration();
			config.forDomainType(CatalogEntity.class).withItemExposure((metadata, httpMethods) ->
				httpMethods.disable(HttpMethod.POST, HttpMethod.PATCH, HttpMethod.PUT, HttpMethod.DELETE));

			config.forDomainType(ProductEntity.class).withItemExposure((metadata, httpMethods) ->
				httpMethods.disable(HttpMethod.POST, HttpMethod.PATCH, HttpMethod.PUT, HttpMethod.DELETE));

	}

	@EventListener(ApplicationReadyEvent.class)
	public void runProductsReloadAfterStartup() {
		catalogService.reloadProducts();
	}

}