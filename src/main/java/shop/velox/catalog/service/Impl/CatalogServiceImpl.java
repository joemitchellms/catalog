package shop.velox.catalog.service.Impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import javax.xml.catalog.CatalogException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import shop.velox.catalog.dao.ProductRepository;
import shop.velox.catalog.model.ProductEntity;
import shop.velox.catalog.service.CatalogService;

@Service
public class CatalogServiceImpl implements CatalogService {

  private static final Logger LOG = LoggerFactory.getLogger(CatalogServiceImpl.class);

  @Value("${catalog.location}")
  private String catalogLocation;

  private ObjectMapper objectMapper;

  private ProductRepository productRepository;

  public CatalogServiceImpl(@Autowired ProductRepository productRepository,
      @Autowired ObjectMapper objectMapper) {
    this.productRepository = productRepository;
    this.objectMapper = objectMapper;
  }

  @Override
  public void reloadProducts() {
    try (FileReader reader = new FileReader(catalogLocation)) {
      List<ProductEntity> products = objectMapper.readValue(reader, new TypeReference<List<ProductEntity>>(){});
      productRepository.deleteAll();
      productRepository.insert(products);
      LOG.info("successfully imported {} products from {}", products.size(), catalogLocation);
    } catch (IOException e) {
      throw new CatalogException("Catalog could not be imported.", e);
    }
  }

}
