package shop.velox.catalog.service;

public interface CatalogService {

  /**
   * reloads the catalog data from json
   */
  void reloadProducts();

}
