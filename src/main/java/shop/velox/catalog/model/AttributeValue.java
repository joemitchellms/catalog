package shop.velox.catalog.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;
import org.springframework.data.mongodb.core.mapping.Field;

@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@SuperBuilder
@FieldDefaults(makeFinal = false, level= AccessLevel.PROTECTED)
@JsonInclude(Include.NON_NULL)
@NoArgsConstructor
@FieldNameConstants
@JsonDeserialize(using = AttributeValueDeserializer.class)
public abstract class AttributeValue<T extends Object> {
  @Field("id")
  @Schema(description = "Human-Readable unique identifier of the AttributeValue.",
      example = "black")
  String id;

  @Schema(description = "value of the AttributeValue.",
      example = "Black")
  T name;
}
