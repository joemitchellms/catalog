package shop.velox.catalog.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;

  @Getter
  @Setter
  @AllArgsConstructor
  @ToString
  @EqualsAndHashCode
  @SuperBuilder
  @FieldDefaults(makeFinal = false, level= AccessLevel.PROTECTED)
  @JsonInclude(Include.NON_NULL)
  @NoArgsConstructor
  @FieldNameConstants
  public class Image {

    @Schema(description = "Image Type")
    ImageType type;

    @Schema(description = "URL of the image")
    String url;
  }
