package shop.velox.catalog.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;
import org.springframework.data.mongodb.core.mapping.Field;


@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@SuperBuilder
@FieldDefaults(makeFinal = false, level= AccessLevel.PROTECTED)
@JsonInclude(Include.NON_NULL)
@NoArgsConstructor
@FieldNameConstants
@JsonDeserialize(using = ChoiceAttributeDeserializer.class)
public class ChoiceAttribute<T extends Object> {
    @Field("id")
    @Schema(description = "Human-Readable unique identifier of the ChoiceAttribute.",
        example = "color")
    String id;

    @Schema(description = "name of the ChoiceAttribute.",
        example = "Color")
    T name;

     @Schema(description = "Actual values of the possible choiceAttributes defined at the Multi Variant Product level.",
        example = "[{id: red, name: Red}, {id: black, name: Black}]")
     List<AttributeValue<T>> values;
}
