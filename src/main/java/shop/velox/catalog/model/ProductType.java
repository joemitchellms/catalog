package shop.velox.catalog.model;

public enum ProductType {
    PRODUCT,
    VARIANT,
    MULTI_VARIANT_PRODUCT
}
