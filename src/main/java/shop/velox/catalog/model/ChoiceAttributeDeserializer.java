package shop.velox.catalog.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import shop.velox.catalog.model.AttributeValue;
import shop.velox.catalog.model.ChoiceAttribute;
import shop.velox.catalog.model.ChoiceAttribute.Fields;
import shop.velox.catalog.model.ChoiceTextAttribute;


@Slf4j
public class ChoiceAttributeDeserializer extends StdDeserializer<ChoiceAttribute> {

  public ChoiceAttributeDeserializer() {
    this(null);
  }

  public ChoiceAttributeDeserializer(Class<?> vc) {
    super(vc);
  }

  @Override
  public ChoiceAttribute<?> deserialize(JsonParser jp, DeserializationContext ctxt)
      throws IOException {
    JsonNode node = jp.getCodec().readTree(jp);
    ObjectMapper mapper = new ObjectMapper();

    String id = node.get(Fields.id).asText();

    JsonNode nameNode = node.get(Fields.name);

    // Add deserialization for Attribute Values
    ArrayNode values = (ArrayNode) node.get(Fields.values);

    if(nameNode.isTextual()) {
      String value = nameNode.textValue();
      List<AttributeValue<String>> attributeValues = Arrays.asList(mapper.readValue(values.toString(), new TypeReference<>(){}));
      return ChoiceTextAttribute.builder().id(id).name(value).values(attributeValues).build();
    }

    throw new IllegalArgumentException("Cannot deserialize nameNode: " + nameNode.toPrettyString());
  }
}