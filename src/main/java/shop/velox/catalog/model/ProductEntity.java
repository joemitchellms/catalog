package shop.velox.catalog.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.List;
import java.util.Map;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@SuperBuilder
@FieldDefaults(makeFinal = false, level= AccessLevel.PROTECTED)
@JsonInclude(Include.NON_NULL)
@NoArgsConstructor
@FieldNameConstants
@Document(collection="products")
public class ProductEntity {

  @Id
  @Schema(description = "Unique identifier of the Product.",
      example = "velox-shirt")
  String id;

  @Schema(description = "Name of the product.",
      example = "Velox Shirt")
  String name;

  @Schema(description = "Description of the product", example = "Very nice VELOX branded Polo-Shirt in size XL.")
  String description;

  @Schema(description = "Long description of the product", example = "Very nice VELOX branded Polo-Shirt in size XL made from cotton in various colours.")
  String longDescription;

  @Schema(description = "Images of the product")
  List<Image> images;

  @Schema(description = "Attributes of Product with no variants")
  Map<String, AttributeValue<?>> attributeValues;

  @Schema(description = "Attributes that change between variants")
  List<ChoiceAttribute<?>> choiceAttributes;

  @Schema(description = "Product Type")
  ProductType type;

  @Schema(description = "Id of a parent MULTI_VARIANT_PRODUCT if a product type is VARIANT", example = "velox-polo-shirt")
  String parentId;

}
