package shop.velox.catalog.dao;

import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;
import shop.velox.catalog.model.CatalogEntity;

@Repository("catalog")
@RepositoryRestResource(collectionResourceRel = "catalogs", path = "catalogs")
@Tag(name = "Catalog Repository", description = "the Catalog Repository API")
public interface CatalogRepository extends MongoRepository<CatalogEntity, String> {

  @Override
  @RestResource(exported = false)
  <S extends CatalogEntity> S save(S entity);



}
