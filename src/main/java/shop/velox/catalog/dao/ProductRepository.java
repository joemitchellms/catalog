package shop.velox.catalog.dao;

import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;
import shop.velox.catalog.model.ProductEntity;

@Repository("products")
@RepositoryRestResource(collectionResourceRel = "products", path = "products")
@Tag(name = "Product Repository", description = "the Product Repository API")
public interface ProductRepository  extends MongoRepository<ProductEntity, String> {

  @Override
  @RestResource(exported = false)
  <S extends ProductEntity> S save(S entity);
}
