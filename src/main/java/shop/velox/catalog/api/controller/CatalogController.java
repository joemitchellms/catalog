package shop.velox.catalog.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

@Tag(name = "Catalog", description = "the Catalog API")
@RequestMapping()
public interface CatalogController {

  @Operation(summary = "reloads catalog data from json File", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201", description = "successful operation")
  })
  @PostMapping(value = "/reloadProducts", produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(HttpStatus.CREATED)
  void reloadProducts();
}
