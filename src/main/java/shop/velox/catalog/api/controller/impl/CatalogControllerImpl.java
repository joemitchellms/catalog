package shop.velox.catalog.api.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import shop.velox.catalog.api.controller.CatalogController;
import shop.velox.catalog.service.CatalogService;

@RestController
public class CatalogControllerImpl implements CatalogController {

  private final CatalogService catalogService;

  public CatalogControllerImpl(@Autowired CatalogService catalogService) {
    this.catalogService = catalogService;
  }

  @Override
  public void reloadProducts() {
    catalogService.reloadProducts();
  }

}
