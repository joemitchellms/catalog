# Catalog Service

More information about the catalog service can be found on the project [wiki](https://gitlab.com/velox-shop/catalog/-/wikis/home) page.

# Work locally
Build Service
```
./gradlew clean build 
```

Execute with Gradle
```
./gradlew bootRun
```

Execute with Docker Compose
```
docker-compose stop && docker-compose rm -fv && docker-compose up --build --force-recreate --remove-orphans
```

Test
```
newman run src/test/*postman_collection.json --environment src/test/veloxCatalog-local.postman_environment.json --reporters cli,html --reporter-html-export newman-results.html
```

## API documentation
Online API Documentation is at:

- HTML: http://localhost:8454/catalog/v1/swagger-ui.html
- JSON: http://localhost:8454/catalog/v1/api-docs

Offline API Documentation is at https://velox-shop.gitlab.io/catalog/swagger/

## Published image versions
Published images are stored in the [veloxswiss/catalog](https://hub.docker.com/r/veloxswiss/catalog) docker hub repository.

|Version      | Description                                                                                                                                          |
|-------------|------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1.0.4       | Update image for velox-coffee-cup product                                                                                                            |
| 1.0.3       | Update articleIds for shirt product (replace 'sly' with 'velox')                                                                                     |
| 1.0.2       | Update images from shirt product variants (add images for red shirts)                                                                                |
| 1.0.1       | Updated ProductEntity: removed Variant class                                                                                                         |
| 1.0.0 (1.0) | Base image                                                                                                                                           |
